// usre of foreach with arrow functions

let names= ['Ania','Dave', 'Hannah', 'Bob']

names.forEach(name => {
    console.log(name + ' is the best')
})

[
    [1, displayWidth+1, displayWidth*2+1, 2], //ltetromino
    [0, displayWidth, displayWidth+1, displayWidth*2+1], //zTetromino
    [1, displayWidth, displayWidth+1, displayWidth+2], //tTetromino
    [0, 1, displayWidth, displayWidth+1], //oTetromino
    [1, displayWidth+1, displayWidth*2+1, displayWidth*3+1] //iTetromino
]