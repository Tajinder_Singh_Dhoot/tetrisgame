document.addEventListener('DOMContentLoaded', () =>{
    const grid = document.querySelector('.grid')
    let squares = Array.from(document.querySelectorAll('.grid div'))
    const scoreDisplay = document.querySelector('#score')
    const startBtn = document.querySelector('#start-button') 
    const width = 10
    let nextRandomShape = 0
    let nextRandomRotation = 0
    console.log(squares);
    let score = 0
    const colors = ['##0341ae', '#72c83b', '#ffd500', '#ff971c', '#ff3213']

    function displayNext() {
        const displaySquares = Array.from(document.querySelectorAll('.mini-grid div'))
        console.log(displaySquares)
        const width = 4
        const lTetromino = [
            [1, width+1, width*2+1, 2],
            [width, width+1, width+2, width*2+2],
            [1, width+1, width*2+1, width*2],
            [width, width*2, width*2+1, width*2+2] 
          ]
        
        const zTetromino = [
            [0,width,width+1,width*2+1],
            [width+1, width+2,width*2,width*2+1],
            [0,width,width+1,width*2+1],
            [width+1, width+2,width*2,width*2+1]
          ]
        
        const tTetromino = [
            [1,width,width+1,width+2],
            [1,width+1,width+2,width*2+1],
            [width,width+1,width+2,width*2+1],
            [1,width,width+1,width*2+1]
          ]
        
        const oTetromino = [
            [0,1,width,width+1],
            [0,1,width,width+1],
            [0,1,width,width+1],
            [0,1,width,width+1]
          ]
        
        const iTetromino = [
            [1,width+1,width*2+1,width*3+1],
            [width,width+1,width+2,width+3],
            [1,width+1,width*2+1,width*3+1],
            [width,width+1,width+2,width+3]
        ]

        const shapes = [lTetromino, zTetromino, tTetromino, oTetromino, iTetromino]
        nextRandomShape = Math.floor(Math.random()*shapes.length)
        nextRandomRotation = Math.floor(Math.random()*4) 
        let nextTetromino = shapes[nextRandomShape][nextRandomRotation]

        displaySquares.forEach(squares => {
            squares.classList.remove('tetromino')
            squares.style.backgroundColor = ''
        })

        nextTetromino.forEach(index => {
            displaySquares[index].classList.add('tetromino')
            displaySquares[index].style.backgroundColor = colors[nextRandomShape]
        })
    }

    const lTetromino = [
        [1, width+1, width*2+1, 2],
        [width, width+1, width+2, width*2+2],
        [1, width+1, width*2+1, width*2],
        [width, width*2, width*2+1, width*2+2] 
      ]
    
    const zTetromino = [
        [0,width,width+1,width*2+1],
        [width+1, width+2,width*2,width*2+1],
        [0,width,width+1,width*2+1],
        [width+1, width+2,width*2,width*2+1]
      ]
    
    const tTetromino = [
        [1,width,width+1,width+2],
        [1,width+1,width+2,width*2+1],
        [width,width+1,width+2,width*2+1],
        [1,width,width+1,width*2+1]
      ]
    
    const oTetromino = [
        [0,1,width,width+1],
        [0,1,width,width+1],
        [0,1,width,width+1],
        [0,1,width,width+1]
      ]
    
    const iTetromino = [
        [1,width+1,width*2+1,width*3+1],
        [width,width+1,width+2,width+3],
        [1,width+1,width*2+1,width*3+1],
        [width,width+1,width+2,width+3]
    ]

    const theTetrominoes = [lTetromino, zTetromino, tTetromino, oTetromino, iTetromino]
    let currentPosition = 4

    //randomly select one of the 5 tetriminos
    let currentTetrimino = Math.floor(Math.random()*theTetrominoes.length)

    // Randomly select one of the 4 directions of a tetrimino
    let currentRotation = Math.floor(Math.random()*4)
    let current = theTetrominoes[currentTetrimino][currentRotation];

    // draw the tetrimino
    function draw() {
        current.forEach(index => {
            squares[currentPosition + index].classList.add('tetromino')
           // document.querySelectorAll('.tetromino').style.backgroundColor = colors[currentTetrimino]
            squares[currentPosition + index].style.backgroundColor = colors[currentTetrimino]
        })
    }
    
    // undraw the Tetrimino
    function undraw() {
        current.forEach(index => {
            squares[currentPosition + index].classList.remove('tetromino');
            squares[currentPosition + index].style.backgroundColor = ''
            //squares[currentPosition + index].style.backgroundColor = colors[random]
        })
    }

    document.querySelector('#start-button').addEventListener('click', startGame)

    function startGame() {
        draw();
        displayNext()
        timerID = setInterval(moveDown, 1000)
    }

    function moveDown() {
        undraw()
        currentPosition += width
        draw()
        freeze()
    }

    //freeze the tetrimino at the bottom
    function freeze() {
        if(current.some(index=> squares[currentPosition + index + width].classList.contains('taken'))) {
            current.forEach(index => squares[currentPosition + index].classList.add('taken'));
            
            currentPosition = 4;
            //randomly select one of the 5 tetriminos
            currentTetrimino = nextRandomShape //Math.floor(Math.random()*theTetrominoes.length);

            // Randomly select one of the 4 shapes of a tetrimino
            currentRotation = nextRandomRotation //Math.floor(Math.random()*4)
            current = theTetrominoes[currentTetrimino][currentRotation];
            draw();
            displayNext()
            addScore()
            gameOver()
        }
    }

    // move the tetromino left unless it is at the edge or there is a blockage
    function moveLeft() {
        undraw()
        const isAtLeftEdge = current.some(index => (currentPosition + index) % width === 0)

        if(!isAtLeftEdge) currentPosition -= 1

        if(current.some(index => squares[currentPosition + index].classList.contains('taken'))) {
            currentPosition += 1
        }
        draw()
    }

    function moveRight() {
        undraw()
        const isAtRightEdge = current.some(index => (currentPosition + index) % width === (width-1))

        if(!isAtRightEdge) currentPosition += 1

        if(current.some(index => squares[currentPosition + index].classList.contains('taken'))) {
           currentPosition -= 1
        }
        draw()
    }

    function rotate() {
        undraw();
        if(currentRotation < 3) {
            current = theTetrominoes[currentTetrimino][++currentRotation];
        } else {
            currentRotation = 0
            current = theTetrominoes[currentTetrimino][currentRotation];
        }
        draw()
    }

    ///add score
    function addScore() {
        for (let i =0; i < 199; i += width) {
            const row = [i, i+1, i+2, i+3, i+4, i+5, i+6, i+7, i+8, i+9]

            if(row.every(index => squares[index].classList.contains('taken'))) {
                score += 10
                document.getElementById('score').innerHTML = score
                row.forEach(index => {
                    squares[index].classList.remove('taken')
                    squares[index].classList.remove('tetromino')
                    squares[index].style.backgroundColor = ''
                })
                let squaresRemoved = squares.splice(i, width)
                squares = squaresRemoved.concat(squares)
                squares.forEach(cell => grid.appendChild(cell))
            }
        }
    }

    function gameOver() {
        if(current.some(index => squares[currentPosition + index].classList.contains('taken'))) {
            document.querySelector('#score').innerHTML = 'end'
            clearInterval(timerID)
        }
    }

    //assign function to keycodes
    function control(e) {
        console.log(e.keyCode);
        if(e.keyCode === 37) {
            moveLeft()
        } else if(e.keyCode === 38) {
            rotate()
        } else if(e.keyCode === 39) {
            moveRight()
        } else if(e.keyCode === 40) {
            moveDown()
        } 
    }
    document.addEventListener('keydown', control)
})